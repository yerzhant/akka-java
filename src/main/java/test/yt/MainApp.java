package test.yt;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;

import static test.yt.TheActor.*;

public class MainApp {
    public static void main(String[] args) {
        final ActorSystem system = ActorSystem.create("main-app");

        final ActorRef theActor = system.actorOf(props(), "the-actor");
        final ActorRef sink = system.actorOf(Sink.props(), "sink");

        theActor.tell(new Say("Hello."), ActorRef.noSender());
        theActor.tell(new DoSomething(), ActorRef.noSender());
        theActor.tell(new Send("Test", sink), ActorRef.noSender());

        system.terminate();
    }
}
