package test.yt;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import test.yt.Sink.BlackHole;

@Slf4j
class TheActor extends AbstractActor {

    @RequiredArgsConstructor
    static class Say {
        public final String message;
    }

    @RequiredArgsConstructor
    static class Send {
        public final String message;
        public final ActorRef sink;
    }

    static class DoSomething {
    }

    static Props props() {
        return Props.create(TheActor.class);
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(Say.class, s -> log.info(s.message))
                .match(DoSomething.class, x -> log.info("Doing something"))
                .match(Send.class, s -> s.sink.tell(new BlackHole(s.message), getSelf()))
                .build();
    }
}
