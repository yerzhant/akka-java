package test.yt;

import akka.actor.AbstractActor;
import akka.actor.Props;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
class Sink extends AbstractActor {

    @RequiredArgsConstructor
    static class BlackHole {
        public final String message;
    }

    static Props props() {
        return Props.create(Sink.class);
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(BlackHole.class, m -> log.info("Swallowing: " + m.message))
                .build();
    }
}
