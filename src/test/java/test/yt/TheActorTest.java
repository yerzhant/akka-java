package test.yt;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.testkit.javadsl.TestKit;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import test.yt.Sink.BlackHole;

import static akka.actor.ActorRef.noSender;
import static org.junit.Assert.assertEquals;
import static test.yt.TheActor.Send;
import static test.yt.TheActor.props;

public class TheActorTest {

    private static ActorSystem system;

    @Before
    public void setUp() {
        system = ActorSystem.create();
    }

    @After
    public void tearDown() {
        TestKit.shutdownActorSystem(system);
        system = null;
    }

    @Test
    public void shouldSendBackAString() {
        final TestKit testKit = new TestKit(system);
        final ActorRef theActor = system.actorOf(props());
        theActor.tell(new Send("The test", testKit.getRef()), noSender());
        assertEquals("The test", testKit.expectMsgClass(BlackHole.class).message);
    }
}